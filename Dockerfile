FROM gentoo/portage:latest as portage
FROM gentoo/stage3:latest

COPY --from=portage /var/db/repos/gentoo /var/db/repos/gentoo

ENV ACCEPT_KEYWORDS="amd64" \
    EMERGE_DEFAULT_OPTS="--jobs=9 --load-average=8 --with-bdeps=y --verbose --getbinpkg" \
    FEATURES="-sandbox -usersandbox -ipc-sandbox -mount-sandbox -network-sandbox -pid-sandbox binpkg-request-signature" \
    MAKEOPTS="-j9 -l8" \
    USE="cairo introspection jpeg tiff"

RUN emerge dev-lang/rust-bin \
&&  emerge app-text/poppler \
    dev-python/pycairo \
    dev-python/pygobject \
    gnome-base/librsvg \
    media-libs/exiftool \
    media-libs/mutagen \
    media-video/ffmpeg \
    x11-libs/gdk-pixbuf \
&&  rm -rf /var/cache/distfiles/* /var/db/repos/*
